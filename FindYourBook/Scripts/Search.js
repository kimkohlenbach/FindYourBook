﻿var api = "https://www.googleapis.com/books/v1/volumes?q=";
var maxItems = "&maxResults=40";
var nrItem = 0;

$("#nextBook").click(function () {
    var searchLink = api + textField.value + maxItems;
    $.ajax({
        type: 'GET',
        url: searchLink,
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (response) {
            nrItem++;
            showBookDetails(nrItem);
        }
    });
});

$("#previousBook").click(function () {
    var searchLink = api + textField.value + maxItems;
    $.ajax({
        type: 'GET',
        url: searchLink,
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (response) {
            nrItem--;
            showBookDetails(nrItem);
        }
    });
});

function hideBookDetails() {
    $("#mainBookContent").css("display", "none");
    $("#paginationButtonsContent").css("display", "none");
    $("#associatedBooksContent").css("display", "none");
    $(".title").css("display", "none");
}

function listBook(response) {

    var BookImage = "BookImage";
    var BookButton = "BookButton";
    var BookNumber = "";
    var completeBookImage = "";
    var completeBookButton = "";

    for (var aux = 1; aux < 25; aux++) {
        BookNumber = aux.toString();
        completeBookImage = BookImage + BookNumber;
        completeBookButton = BookButton + BookNumber;
        document.getElementById(completeBookButton).text = response.items[nrItem + aux].volumeInfo.title;
        document.getElementById(completeBookImage).src = response.items[nrItem + aux].volumeInfo.imageLinks !== undefined ? response.items[nrItem + aux].volumeInfo.imageLinks.thumbnail : 'https://books.google.com.br/googlebooks/images/no_cover_thumb.gif';
        document.getElementById(completeBookButton).href = response.items[nrItem + aux].volumeInfo.previewLink;
    }
}

function showBookDetails(nrItem) {
    clearInformations();
    var searchLink = api + textField.value + maxItems;
    $.ajax({
        type: 'GET',
        url: searchLink,
        data: { get_param: 'value' },
        dataType: 'json',
        success: function (response) {
            $("#mainBookContent").css("display", "block");
            $("#paginationButtonsContent").css("display", "block");
            $("#associatedBooksContent").css("display", "block");
            $("#priceBook").css("display", "block");
            document.getElementById("imageBook").src = response.items[nrItem].volumeInfo.imageLinks !== undefined ? response.items[nrItem].volumeInfo.imageLinks.thumbnail : 'https://books.google.com.br/googlebooks/images/no_cover_thumb.gif';
            $("#titleBook").text(response.items[nrItem].volumeInfo.title);
            $("#authorsBook").text(response.items[nrItem].volumeInfo.authors);
            $("#categoryBook").text(response.items[nrItem].volumeInfo.categories);
            $("#descriptionBook").text(response.items[nrItem].volumeInfo.description);
            $("#previewLinkBook").attr("href", response.items[nrItem].volumeInfo.previewLink);

            if (response.items[nrItem].saleInfo.buyLink != undefined) {
                $("#BookButton0").attr("href", response.items[nrItem].saleInfo.buyLink);
                $("#priceBook").text("R$" + response.items[nrItem].saleInfo.retailPrice.amount);
            }
            else {
                $("#BookButton0").hide();
                $("#priceBook").hide();
            }
        }
    });
}

function clearInformations() {
    $("#titleBook").empty();
    $("#authorsBook").empty();
    $("#categoryBook").empty();
    $("#descriptionBook").empty();
    $("#previewLinkBook").prop("href", "");
    document.getElementById("priceBook").text = "";
    document.getElementById("imageBook").src = "";
    document.getElementById("BookButton0").href = "";
}

function actions() {
    $("#searchBook").click(function () {
        clearInformations();
        var searchLink = api + textField.value + maxItems;
        nrItem = 0;
        $("#textSearch").text(textField.value);
        $.ajax({
            type: 'GET',
            url: searchLink,
            data: { get_param: 'value' },
            dataType: 'json',
            success: function (response) {

                if (response.totalItems != 0) {
                    showBookDetails(nrItem);
                    listBook(response);
                    $(".alert").css("display", "none");
                    $(".alert-link").css("display", "none");
                }
                else {
                    hideBookDetails();
                    $(".alert").css("display", "block");
                    $(".alert-link").css("display", "block");
                }
            }
        });
    });
}

$(document).ready(function () {
    actions();
});